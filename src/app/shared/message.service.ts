import { Injectable } from '@angular/core';
import { Message } from './message.model';
import { Subject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';


@Injectable()
export class MessageService{
  messagesFetching = new Subject<boolean>();
  messagesUpLoading = new Subject<boolean>();

  private allMessages: Message[] = [];

  constructor(private http: HttpClient) {}

  getAllMessages(){
    return this.allMessages.slice();
  }
  addMessages(message: Message){
    const body = new HttpParams()
      .set('message', `${message.message}`)
      .set('author', `${message.author}`)
      .set('date', `${message.datetime}`)
    this.messagesUpLoading.next(true);
   return this.http.post(' http://146.185.154.90:8000/messages', body)
      .pipe(
        tap(() => {
            this.messagesUpLoading.next(false)
          }, () => {
            this.messagesUpLoading.next(false)
          }
        )
      )
  };

  start(){
    this.messagesFetching.next(true);
    return  this.http.get<{[id: string]: Message}>('http://146.185.154.90:8000/messages?datetime=2022-01-01T10:20:39.586Z')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const messageData = result[id];
          console.log(result)
          return new Message(
            messageData.id, messageData.message,
            messageData.author, messageData.datetime
          )
        })
      }))
  }


}
