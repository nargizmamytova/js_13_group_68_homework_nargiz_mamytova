import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageService } from '../shared/message.service';
import { Message } from '../shared/message.model';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit , OnDestroy{
  allMessages!: Message[];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscription!: Subscription;
  loading: boolean = false;
  constructor( private messageService: MessageService) { }

  ngOnInit(): void {
    this.allMessages = this.messageService.getAllMessages();

    setInterval(() => {
     this.messagesChangeSubscription = (this.messageService.start().subscribe((messages: Message[]) => {
       this.allMessages = messages;
       this.allMessages.forEach(message => {
       })
       this.loading = false;
     }))
      this.messagesFetchingSubscription = (this.messageService.messagesFetching.subscribe((isFetching: boolean) => {
        this.loading = isFetching;
      }))
    }, 5000)
  }
  ngOnDestroy() {
    this.messagesChangeSubscription.unsubscribe()
  }
}
