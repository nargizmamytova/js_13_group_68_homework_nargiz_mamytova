import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageService } from '../shared/message.service';
import { Message } from '../shared/message.model';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent implements OnInit {
  @ViewChild('f') messageForm!: NgForm;
  isUpLoading: boolean = false;

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {

  }
  createMessage() {
    this.isUpLoading = true;
      const id = Math.random().toString();
      const date = new Date().toDateString();
      const message = new Message(
        id,
        this.messageForm.value.message,
        this.messageForm.value.author,
        date,

      );
      const next = () => {
        this.messageService.start();
        this.setFormValue({
          message: '',
          author : '',

        })
        this.isUpLoading = false;

      };
      this.messageService.addMessages(message).subscribe(next);

  }
  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.messageForm.form.setValue(value);
    })
  }
}
